# -*- coding: utf-8 -*-

from django.contrib import admin

from models import *


def update_title(modeladmin, request, queryset):
    queryset.retrieve_titles()

update_title.short_description = u'Обновить заголовки'

class LinkAdmin(admin.ModelAdmin):
    list_display = ['url', 'title', 'last_updated']
    ordering = ['-last_updated']
    exclude = ('last_updated', 'title')
    actions = [update_title]

admin.site.register(Link, LinkAdmin)

