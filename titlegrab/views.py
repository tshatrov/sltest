# -*- coding: utf-8 -*-

from django.shortcuts import render

from models import Link

# Create your views here.

def index(request):
    ctx = {'links': Link.objects.all().order_by('-last_updated')}
    return render(request, 'index.html', ctx)
