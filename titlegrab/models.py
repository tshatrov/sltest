# -*- coding: utf-8 -*-

from lxml import etree
from lxml.cssselect import CSSSelector
import unirest

from django.utils import timezone
from django.conf import settings
from django.db import models


class LinkQuerySet(models.QuerySet):
    def retrieve_titles(self):
        u''' Retrieve titles for links in the queryset '''
        threads = (link.retrieve_title_async() for link in self)
        for thread in threads:
            thread.join(settings.REQUEST_TIMEOUT)


class LinkManager(models.Manager):
    def get_queryset(self):
        return LinkQuerySet(self.model, using=self._db)


class Link(models.Model):
    objects = LinkManager()
    url = models.CharField(u'URL', max_length=512)
    last_updated = models.DateTimeField(u'Последнее обновление', null=True)
    title = models.CharField(u'Заголовок', max_length=1000, null=True)
    
    def __unicode__(self):
        return u'Ссылка ({})'.format(self.url)

    def update_title(self, title):
        self.title = title
        self.last_updated = timezone.now()
        self.save()

    def parse_title(self, response):
        u''' Parse page title based on response '''
        if response.code == 200:
            content = etree.HTML(response.body)
            title_list = CSSSelector('head > title')(content)
            if title_list:
                title = title_list[0].text
                self.update_title(title)
                return title

    def retrieve_title_async(self):
        ''' Retrieve and update page title; returns a thread '''
        return unirest.get(self.url, callback=self.parse_title)

    def retrieve_title(self):
        self.retrieve_title_async().join(settings.REQUEST_TIMEOUT)

    @classmethod
    def retrieve_all_titles(cls):
        threads = (link.retrieve_title_async() for link in cls.objects.all())
        for thread in threads:
            thread.join(settings.REQUEST_TIMEOUT)
    