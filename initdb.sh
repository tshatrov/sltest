#!/bin/bash

./manage.py migrate
./manage.py loaddata initial_data
echo "Creating admin superuser..."
./manage.py createsuperuser
